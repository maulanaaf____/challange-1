//Maulana Afif
//Universitas Sebelas Maret
package com.company;
import javax.jws.soap.SOAPBinding;
import java.util.Scanner;
import java.util.*;
public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner pilihUser;
        pilihUser = new Scanner(System.in);
        System.out.println("Maulana Afif_Universitas Sebelas Maret");
        System.out.println("--------------------------------------");
        System.out.println("Apakah Anda ingin menggunakan calculator ? (Y/N)");
        char pilihPilihan;
        pilihPilihan = pilihUser.next().charAt(0);
        System.out.println("pilihan adalah " + pilihPilihan);
        while (pilihPilihan == 'Y') {
            System.out.println("--------------------------------------");
            System.out.println(" Kalkulator Penghitung Luas dan Volume ");
            System.out.println("--------------------------------------");
            System.out.println("Menu : ");
            System.out.println("1. Hitung Luas Bidang ");
            System.out.println("2. Hitung Volume Bidang \n");
            System.out.println("--------------------------------------");


            Scanner input = new Scanner(System.in);
            System.out.print("Masukkan Nomor sesuai dengan perintah yang ingin Anda jalankan = ");
            int number = input.nextInt();

            //Menghitung Luas Bidang
            if (number == 1) {
                System.out.println("\n--------------------------------------");
                System.out.println("Pilih bidang yang akan di hitung");
                System.out.println("1. Persegi");
                System.out.println("2. Lingkaran");
                System.out.println("3. Segitiga");
                System.out.println("4. Persegi panjang");
                //System.out.println("0. kembali ke menu sebelumnya");
                System.out.println("--------------------------------------");

                Scanner inputUser;
                inputUser = new Scanner(System.in);
                String Pilihan;
                System.out.print("Masukkan bidang yang ingin dihitung luasnya = ");
                Pilihan = inputUser.next();

                switch (Pilihan) {
                    case "1":
                        float sisi, LuasPersegi;
                        System.out.println("\n--------------------------------------");
                        System.out.print("\n Masukkan panjang sisi persegi (cm) = ");
                        sisi = inputUser.nextFloat();
                        LuasPersegi = sisi * sisi;
                        System.out.println("Luas persegi adalah " + LuasPersegi + " cm2");
                        System.out.println("\n--------------------------------------");
                        break;
                    case "2":
                        double JariJari, LuasLingkaran;
                        System.out.println("\n--------------------------------------");
                        System.out.print("\nMasukkan panjang jari-jari lingkaran (cm) = ");
                        JariJari = inputUser.nextFloat();
                        LuasLingkaran = JariJari * 22 * JariJari / 7;
                        System.out.println("Luas lingkaran adalah " + LuasLingkaran + " cm2");
                        System.out.println("\n--------------------------------------");
                        break;
                    case "3":
                        float alas, tinggi, LuasSegitiga;
                        System.out.println("\n--------------------------------------");
                        System.out.print("\nMasukkan panjang alas segitiga (cm) = ");
                        alas = inputUser.nextFloat();
                        System.out.print("Masukkan panjang tinggi segitiga (cm) = ");
                        tinggi = inputUser.nextFloat();
                        LuasSegitiga = alas * tinggi / 2;
                        System.out.println("Luas persegi adalah " + LuasSegitiga + " cm2");
                        System.out.println("\n--------------------------------------");
                        break;
                    case "4":
                        float panjang, lebar = 0, LuasPersegiPanjang;
                        System.out.println("\n--------------------------------------");
                        System.out.print("\nMasukkan nilai panjang (cm) = ");
                        panjang = inputUser.nextFloat();
                        System.out.print("Masukkan nilai lebar (cm) = ");
                        lebar = inputUser.nextFloat();
                        LuasPersegiPanjang = panjang * lebar;
                        System.out.println("Luas persegi panjang adalah " + LuasPersegiPanjang + " cm2");
                        System.out.println("\n--------------------------------------");
                        break;
                }

            } else if (number == 2) {
                System.out.println("\n Volume apa yang ingin Anda cari ");
                System.out.println("\n--------------------------------------");
                System.out.println("Pilih bidang yang akan di hitung");
                System.out.println("1. Kubus");
                System.out.println("2. Balok");
                //System.out.println("0. kembali ke menu sebelumnya");
                System.out.println("--------------------------------------");
                Scanner inputUser;
                inputUser = new Scanner(System.in);
                String Pilihan;
                System.out.print("Masukkan bangun yang ingin dihitung volumenya = ");
                Pilihan = inputUser.next();

                switch (Pilihan) {
                    case "1":
                        float sisiKubus, VolumeKubus;
                        System.out.println("\n--------------------------------------");
                        System.out.print("\nMasukkan panjang sisi kubus (cm) = ");
                        sisiKubus = inputUser.nextFloat();
                        VolumeKubus = sisiKubus * sisiKubus * sisiKubus;
                        System.out.println("Volume Kubus adalah " + VolumeKubus + " cm3");
                        System.out.println("\n--------------------------------------");
                        break;
                    case "2":
                        double Panjang, Lebar, Tinggi, VolumeBalok;
                        System.out.println("\n--------------------------------------");
                        System.out.print("\nMasukkan nilai panjang (cm) = ");
                        Panjang = inputUser.nextFloat();
                        System.out.print("Masukkan nilai lebar (cm) = ");
                        Lebar = inputUser.nextFloat();
                        System.out.print("Masukkan nilai tinggi (cm) = ");
                        Tinggi = inputUser.nextFloat();
                        VolumeBalok = Panjang * Lebar * Tinggi;
                        System.out.println("Volume balok adalah " + VolumeBalok + " cm3");
                        System.out.println("\n--------------------------------------");
                        break;
                }

            }
            System.out.println("Apakah Anda ingin menggunakan kalkulator lagi ? (Y/N)");
            pilihPilihan = pilihUser.next().charAt(0);
        }

    }


}
